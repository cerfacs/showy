# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).



## [0.1.1] 2021 / 11 / 26

### Changed
* better handling of legend



## [0.1.0] 2021 / 11 / 26

### Added
* schema validation
* multiple curve visualization (>2)

### Fixed
* display of wildcard based axes labels

### Changed
* simpler handling of wild card manipulations
* better handling of legend for multiple pictures
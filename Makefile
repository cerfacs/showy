coverage:
	pytest --cov=showy

test:
	pytest --cov=showy 


doc:
	cd docs && make html

wheel:
	rm -rf build
	rm -rf dist
	python setup.py sdist bdist_wheel

upload_test:
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*

upload:
	twine upload dist/*

clean:
	rm -rf docs/_build
	rm -rf docs/source/api/_generated

import numpy as np

from showy import showy


def test_showy():
    data = dict()
    data['time'] = time = np.linspace(0, 0.1, num=256)

    for freq in np.linspace(10, 20, num=9):
        data[f'sine_{freq}'] = np.sin(time * freq * 2 * np.pi)
        data[f'cosine_{freq}'] = np.cos(time * freq * 2 * np.pi)
    data['cos'] = np.cos(time)

    # Creating a template
    template = {
        "title": "Example",
        "graphs": [
            {"curves": [
                {"var": "sine_*"}, {"var": "cosine_*"}, {"var": "cos"}],
             "x_var": "time",
             "y_label": "Sine [mol/m³/s]",
             "x_label": "Time [s]",
             "title": "Sine and cosine of frequency *"},
            {"curves": [
                {"var": "cosine_10.0"}, {"var": "sine_10.0"}],
             "x_var": "time"}
        ],
        "figure_structure": [3, 3],
        "figure_dpi": 92.6
    }

    # single data
    showy(template, data, show=False)

    # multiple data
    showy(template, [data, data], show=False)
